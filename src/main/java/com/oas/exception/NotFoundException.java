package com.oas.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotFoundException extends AbstractException {
	private static final long serialVersionUID = -7803921948374255994L;
	public NotFoundException(ErrorCode errorCode) {
		super(errorCode);
	}


}
