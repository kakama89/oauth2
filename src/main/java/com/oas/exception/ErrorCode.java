package com.oas.exception;

public enum ErrorCode {
	ERR_00001("Entity not found"),
	ERR_00002("Entity already exists"),
	WARN_00001(""), 
	INF_00001(""), ;

	private String message;

	private ErrorCode(String message) {
		this.message = message;
	}

	public String message(){
		return message;
	}
	@Override
	public String toString() {
		return name();
	}
}
