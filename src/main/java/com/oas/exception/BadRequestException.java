package com.oas.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BadRequestException extends AbstractException{
	private static final long serialVersionUID = 3857985223575899547L;

	public BadRequestException(ErrorCode errorCode) {
		super(errorCode);
	}
}
