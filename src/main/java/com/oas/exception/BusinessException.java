package com.oas.exception;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BusinessException extends AbstractException {
	private static final long serialVersionUID = -3490702231990216868L;
	public BusinessException(ErrorCode errorCode) {
		super(errorCode);
	}
}
