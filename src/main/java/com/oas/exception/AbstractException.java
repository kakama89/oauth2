package com.oas.exception;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public abstract class AbstractException extends RuntimeException{
	private static final long serialVersionUID = 7101576005176041254L;
	
	@NotNull
	private ErrorCode errorCode;

}
