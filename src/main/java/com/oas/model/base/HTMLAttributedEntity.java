package com.oas.model.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class HTMLAttributedEntity {
	@Column(name = "width_pos")
	private Float width;
	
	@Column(name = "height_pos")
	private Float height;
	
	@Column(name = "top_pos")
	private Float top;
	
	@Column(name = "left_pos")
	private Float left;
	
	@Column(name = "style")
	private String style;
}
