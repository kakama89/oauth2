package com.oas.model.base;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.time.Instant;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity<U> {
	@CreatedBy
	@Column(name = "created_by")
	protected U createdBy;
	
	@CreatedDate
	@Column(name = "created_date")
	protected Instant createdDate;
	
	@LastModifiedBy
	@Column(name = "last_modified_by")
	protected U lastModifiedBy;
	
	@LastModifiedDate
	@Column(name = "last_modified_date")
	protected Instant lastModifiedDate;

	@Version
	@Column(name = "version")
	private Long version;
}
