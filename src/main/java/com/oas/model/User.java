package com.oas.model;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.base.Objects;
import com.oas.model.base.BaseEntity;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class User extends BaseEntity<String> implements UserDetails {
	private static final long serialVersionUID = -7087493464568875475L;
	public User(String username, String password, UserStatus status){
		this.username = username;
		this.password = password;
		this.status = status;
	}
	
	public User(String username, String password, String status){
		this(username, password, UserStatus.valueOf(status));
	}

	public enum UserStatus {
		ACTIVE, INACTIVE, LOCKED, EXPIRED
	}

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", unique = true, updatable = false, nullable = false)
	private String id;

	@Column
	private String username;

	@Column
	private String password;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private UserStatus status;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_authority", 
		joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") }, 
		inverseJoinColumns = { @JoinColumn(name = "authority_id", referencedColumnName = "id") })
	private Set<Authority> roles;

	@Override
	public Set<SimpleGrantedAuthority> getAuthorities() {
		return roles.stream().map(Authority::getCode)
				.map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
	}

	@Override
	public boolean isAccountNonExpired() {
		return !UserStatus.EXPIRED.equals(status);
	}

	@Override
	public boolean isAccountNonLocked() {
		return !UserStatus.LOCKED.equals(status);
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return UserStatus.ACTIVE.equals(status);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(username);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final User user = (User) obj;
		return Objects.equal(this.username, user.getUsername());
	}

}
