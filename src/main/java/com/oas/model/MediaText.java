package com.oas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.oas.model.base.Media;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "media_text")
public class MediaText extends Media {
	@Column(name = "text")
	private String text;
}
