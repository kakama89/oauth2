package com.oas.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.oas.model.base.HTMLAttributedEntity;
@Getter
@Setter
@Entity
@Table(name = "greeting_page")
public class GreetingPage extends HTMLAttributedEntity{
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	private Greeting greeting;
	
	@OneToMany(mappedBy = "page", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MediaText> mediaTexts = new ArrayList<>();
	
	@OneToMany(mappedBy = "page", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MediaImage> mediaImages = new ArrayList<>();
	
	@Column(name = "page")
	private Integer page;
	
	@Column(name = "background_image")
	private String backgroundImage;
	
	@Column(name = "background_color")
	private String backgroundColor;
}
