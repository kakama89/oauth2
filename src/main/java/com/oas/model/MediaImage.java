package com.oas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.oas.model.base.Media;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "media_image")
public class MediaImage extends Media {
	@Column
	private String background;
}
