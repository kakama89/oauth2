package com.oas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.oas.model.base.BaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "language")
public class Language extends BaseEntity<String> {
	public enum Status {
		ACTIVE, INACTIVE
	}

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "code", nullable=false, unique = true, length = 2)
	private String code;

	@Column(name = "name", length = 50, nullable = false)
	private String name;
	
	@Column(name = "name_eng", length = 50, nullable = false)
	private String nameEng;

	@Column(name = "status", length = 10, nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;

}
