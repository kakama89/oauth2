package com.oas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.oas.model.base.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "i18n_message", 
		uniqueConstraints = @UniqueConstraint(columnNames = {"code", "locale" }),
		indexes = {@Index(columnList = "code,locale", unique = true)})

public class I18nMessage extends BaseEntity<String> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String locale;

	@Column
	private String code;

	@Column
	private String message;

}
