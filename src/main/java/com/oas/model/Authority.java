package com.oas.model;

import javax.persistence.*;

import org.springframework.security.core.GrantedAuthority;

import com.oas.model.base.BaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "authority")
public class Authority extends BaseEntity<String> implements GrantedAuthority {
	private static final long serialVersionUID = -647758024098474613L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 50, unique = true, nullable = false)
	private String code;

	@Column(length = 50)
	private String name;

	@Override
	public String getAuthority() {
		return code;
	}
}
