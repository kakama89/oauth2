package com.oas.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.oas.model.base.BaseEntity;

@Entity
@Table(name = "greeting")
@Getter
@Setter
public class Greeting extends BaseEntity<String> {
	public enum Status {
		ACTIVE, NEW	, INACTIVE
	}

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", unique = true, updatable = false, nullable = false)
	private String id;
	
	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "thumbnail")
	private String thumbnail;
	
	@Column(name = "status", length = 10)
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@ManyToOne
	private GreetingCategory category;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "greeting")
	private List<GreetingPage> pages = new ArrayList<>();
	
	@Column(name = "likes")
	private Long numberOfLike;

	@Column(name = "views")
	private Long numberOfView;

	@Column(name = "picks")
	private Long numberOfPick;

	@Column(name = "downloadable")
	private boolean downloadAble;

	@Column(name = "printable")
	private boolean printAble;

	@Column(name = "send_online")
	private boolean sendOnline;

}
