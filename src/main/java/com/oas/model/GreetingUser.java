package com.oas.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.oas.model.base.BaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "greeting_user")
@EntityListeners(AuditingEntityListener.class)
public class GreetingUser extends BaseEntity<String>{
	public enum Status {
		ACTIVE, PENDING, EXPIRED
	}
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", unique = true, updatable = false, nullable = false)
	private String id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "thumbnail")
	private String thumbnail;
	
	@Column(name = "event_date")
	private Instant eventDate;

	@ManyToOne
	private GreetingCategory category;

	@Type(type = "text")
	@Column(name = "content", columnDefinition = "text")
	private String content;

	@Column(name = "status", length = 10)
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Column(name = "downloadable")
	private boolean downloadAble;

	@Column(name = "printable")
	private boolean printAble;

	@Column(name = "send_online")
	private boolean sendOnline;
}
