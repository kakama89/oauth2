package com.oas.business.user.greeting;

import java.util.Locale;



public interface GreetingUserService{
	GreetingUserDTO getGreetingUser(String id);
	GreetingUserDTO createGreetingUser(GreetingUserDTO payload);
	GreetingUserDTO toLoccale(GreetingUserDTO origin, Locale locate);
}
