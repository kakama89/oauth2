package com.oas.business.user.greeting;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/u/greetings")
public class GreetingUserController {

	@GetMapping("/{id}")
	public ResponseEntity<GreetingUserDTO> getUserGreeting(@PathVariable String id){
		return null;
	}
		
	@PutMapping("/{id}")
	public ResponseEntity<GreetingUserDTO> updateUserGreeting(@PathVariable String  id, @Valid @RequestBody GreetingUserDTO payload){
		return null;
	}
}
