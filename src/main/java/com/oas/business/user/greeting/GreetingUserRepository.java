package com.oas.business.user.greeting;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oas.model.GreetingUser;

@Repository
public interface GreetingUserRepository extends JpaRepository<GreetingUser, String>{

}
