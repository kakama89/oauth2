package com.oas.business.user.greeting;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oas.exception.ErrorCode;
import com.oas.exception.NotFoundException;
import com.oas.i18n.I18nService;
import com.oas.model.GreetingUser;

@Service
public class GreetingUserServiceImpl implements GreetingUserService{
	private final I18nService i18nService;
	private final GreetingUserRepository greetingUserRepository;
	
	@Autowired
	public GreetingUserServiceImpl(GreetingUserRepository greetingUserRepository, I18nService i18nService){
		this.greetingUserRepository = greetingUserRepository;
		this.i18nService = i18nService;
	}
	
	@Override
	@Transactional(readOnly = true)
	public GreetingUserDTO getGreetingUser(String id) {
		GreetingUser greeting = greetingUserRepository.findById(id)
				.orElseThrow(()-> new NotFoundException(ErrorCode.ERR_00001));
		return GreetingUserMapper.entityToDTO(greeting);
	}
	
	@Override
	@Transactional(readOnly = false)
	public GreetingUserDTO createGreetingUser(GreetingUserDTO payload) {
		GreetingUser greeting = GreetingUserMapper.dtoToEntity(payload);
		greetingUserRepository.save(greeting);
		return GreetingUserMapper.entityToDTO(greeting);
	}

	@Override
	public GreetingUserDTO toLoccale(GreetingUserDTO origin, Locale locale) {
		return null;
	}

}
