package com.oas.business.user.greeting;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GreetingUserDTO{
	private String id;
	private String title;
	private String thumbnail;
	private Long eventDate;
	private String content;
	private boolean downloadAble;
	private boolean printAble;
	private boolean sendOnline;
}
