package com.oas.business.user.greeting;

import java.time.Instant;
import java.util.Optional;

import com.oas.model.GreetingUser;

public class GreetingUserMapper {
	private GreetingUserMapper() {
		
	}
	public static GreetingUserDTO entityToDTO(GreetingUser entity){
		return GreetingUserDTO.builder()
			.id(entity.getId())
			.title(entity.getTitle())
			.thumbnail(entity.getThumbnail())
			.content(entity.getContent())
			.eventDate(Optional.ofNullable(entity.getEventDate()).map(Instant::toEpochMilli).orElse(null))
			.printAble(entity.isPrintAble())
			.downloadAble(entity.isDownloadAble())
			.sendOnline(entity.isSendOnline())
			.build();
	}
	
	public static GreetingUser dtoToEntity(GreetingUserDTO payload){
		GreetingUser greeting = new GreetingUser();
		greeting.setId(payload.getId());
		greeting.setTitle(payload.getTitle());
		greeting.setThumbnail(payload.getThumbnail());
		greeting.setContent(payload.getContent());
		greeting.setDownloadAble(payload.isDownloadAble());
		greeting.setPrintAble(payload.isPrintAble());
		greeting.setSendOnline(payload.isSendOnline());
		greeting.setEventDate(Instant.ofEpochSecond(payload.getEventDate()));
		return greeting;
	}
}
