package com.oas.business.user.account;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.oas.system.config.security.oauth2.Oauth2Properties;
import com.oas.web.WebUtil;

@RestController
@RequestMapping("/u/accounts")
public class UserController {
	private final Oauth2Properties oauth2Properties;

	@Autowired
	public UserController(Oauth2Properties oauth2Properties) {
		this.oauth2Properties = oauth2Properties;
	}
	
	@PostMapping("/auth")
	public ResponseEntity<OAuth2AccessToken> auth(@Valid @RequestBody LoginDTO payload){
		WebClient  client = WebClient.builder().baseUrl(WebUtil.baseUrl()).build();
		Map<String, String> params = new HashMap<>();
		params.put("username", payload.getUsername());
		params.put("password", payload.getPassword());
		params.put("grant_type", "password");
	
		OAuth2AccessToken token = client.post().uri("/oauth/token")
		.header("Authorization", WebUtil.getBasicAuth(oauth2Properties.getClientId(), oauth2Properties.getClientSecret()))
		.body(WebUtil.body(params))
		.retrieve()
		.bodyToMono(OAuth2AccessToken.class).block();
		return ResponseEntity.ok(token);
	}
}
