package com.oas.business.user.account;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class RefreshTokenDTO {
	@NotBlank
	@JsonProperty("refresh_token")
	private String refreshToken;
}
