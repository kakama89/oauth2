package com.oas.business.user.account;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oas.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
	Optional<User> findByUsernameIgnoreCase(final String username);
	boolean existsByUsername(final String username);
	boolean existsByUsernameAndIdNot(final String username, String id);
}
