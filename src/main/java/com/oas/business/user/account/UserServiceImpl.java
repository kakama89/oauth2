package com.oas.business.user.account;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
	private final UserRepository userRepo;
	
	public UserServiceImpl(final UserRepository userRepo) {
		this.userRepo = userRepo;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username){
		return this.userRepo.findByUsernameIgnoreCase(username)
				.orElseThrow(()-> new UsernameNotFoundException("Username not found"));
	}

}
