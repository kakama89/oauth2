package com.oas.business.user.account;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService{
}
