package com.oas.business;

import java.io.Serializable;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

public interface CrudService<T, I extends Serializable , D> {
	T create(D payload, Function<D, T> func);
//	Optional<T> update(I pk , D payload, BiFunction<D, T, T> func);
	Optional<T> update(I pk , D payload, BiConsumer<D, T> consumer);
	Optional<T> get(I pk);
	Optional<D> get(I pk, Function<T, D> func);
	void delete(I pk);
}
