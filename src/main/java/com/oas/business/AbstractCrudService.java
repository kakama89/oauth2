package com.oas.business;

import java.io.Serializable;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public abstract class AbstractCrudService<T, I extends Serializable, R extends JpaRepository<T, I>, D> implements CrudService<T, I, D>{
	
	protected final R repository;
	
	public AbstractCrudService(R repository) {
		this.repository = repository;
	}
	
	@Override
	public Optional<T> get(I pk) {
		return repository.findById(pk); 
	}
	
	@Override
	public Optional<D> get(I pk, Function<T, D> func) {
		Optional<T> opt = repository.findById(pk);
		if(opt.isPresent()) {
			T entity = opt.get();
			return Optional.of(func.apply(entity));
		}
		return Optional.empty();
	}
	
	@Override
	@Transactional(readOnly = false)
	public T create(D payload, Function<D, T> mapFunc) {
		T entity = mapFunc.apply(payload);
		return repository.save(entity);
	}

//	@Override
//	@Transactional(readOnly = false)
//	public Optional<T> update(I pk, D payload, BiFunction<D, T, T> func) {
//		Optional<T> opt = get(pk);
//		if(opt.isPresent()) {
//			T entity = func.apply(payload, opt.get());
//			return Optional.of(repository.save(entity));
//		}
//		return Optional.empty();
//	}
	
	@Override
	@Transactional(readOnly = false)
	public Optional<T> update(I pk, D payload, BiConsumer<D, T> consumer) {
		Optional<T> opt = get(pk);
		if(opt.isPresent()) {
			T entity = opt.get();
			consumer.accept(payload, entity);
			return Optional.of(repository.save(entity));
		}
		return Optional.empty();
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(I pk) {
		repository.deleteById(pk);
	}
}
