package com.oas.business.aws;

import java.io.IOException;
import java.net.URL;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/aws/s3")
public class AwsS3Controller {
	private final AwsS3Service awsS3Service;
	public AwsS3Controller(AwsS3Service awsS3Service) {
		this.awsS3Service = awsS3Service;
	}
	@PostMapping("/upload")
	public String uploadFile(@RequestParam String bucketName, @RequestPart MultipartFile file) throws IOException{
		URL url = awsS3Service.upload(bucketName, file);
		return url.toString();
	}

}
