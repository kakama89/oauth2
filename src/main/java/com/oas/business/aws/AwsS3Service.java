package com.oas.business.aws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;

@Service
public class AwsS3Service {
	private final AmazonS3 amazoneS3;
	private final AmazonS3Property s3Property;
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMddHHmmssZ");
	
	public AwsS3Service(final AmazonS3 amazoneS3, final AmazonS3Property s3Property) {
		this.amazoneS3 = amazoneS3;
		this.s3Property = s3Property;
	}

	
	public URL upload(String bucketName, MultipartFile mFile) throws IOException{
		File file = multiPartToFile(mFile);
		return uploadFileToBucket(bucketName, file);
	}
	
	public URL uploadFileToBucket(String bucketName, File file) throws IOException {
		if(!amazoneS3.doesBucketExistV2(bucketName)) {
			amazoneS3.createBucket(bucketName);
			return null;
		}
		String fileName = file.getName();
		String baseName = FilenameUtils.getBaseName(fileName);
		String ext = FilenameUtils.getExtension(fileName);
		String key = baseName + FORMAT.format(new Date()) + "." + ext;
		amazoneS3.putObject(bucketName, key, file);
		return amazoneS3.getUrl(bucketName, key);
	}
	public File multiPartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		try(FileOutputStream fos = new FileOutputStream(convFile);){
			fos.write(file.getBytes());
		    return convFile;	
		}
	}
	
}
