package com.oas.business.aws;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Configuration
@ConfigurationProperties(value = "awss3")
public class AmazonS3Property {
	private String accessKey;
	private String secretKey;
	private String regionName;
	private String bucketName;
	private String endPoint;
}
