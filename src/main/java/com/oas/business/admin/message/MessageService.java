package com.oas.business.admin.message;

import java.util.List;

import com.oas.dto.KeyValuePairDTO;

public interface MessageService {
	void createMessageKey(final List<KeyValuePairDTO<String, MessageLanguageDTO>> mgs);
}
