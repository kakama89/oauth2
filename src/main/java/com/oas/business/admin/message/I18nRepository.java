package com.oas.business.admin.message;

import com.oas.model.I18nMessage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface I18nRepository extends JpaRepository<I18nMessage, Long> {
    Optional<I18nMessage> findByCodeAndLocale(final String code, final String locale);
}
