package com.oas.business.admin.message;

import com.oas.dto.KeyValuePairDTO;
import com.oas.model.I18nMessage;

public class I18nMessageMapper {
	public static I18nMessage dtoToEntity(KeyValuePairDTO<String, MessageLanguageDTO> payload){
		I18nMessage i18m = new I18nMessage();
		i18m.setCode(payload.getKey());
		MessageLanguageDTO lang = payload.getValue();
		i18m.setLocale(lang.getLanguageCode());
		i18m.setMessage(lang.getMessage());
		return i18m;
	}
}
