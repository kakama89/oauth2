package com.oas.business.admin.message;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageLanguageDTO {
	@NotNull
	private String message;
	
	@NotNull
	private Long languageId;
	
	@NotNull
	private String languageCode;
}
