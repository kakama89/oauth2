package com.oas.business.admin.message;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oas.business.AbstractCrudService;
import com.oas.dto.KeyValuePairDTO;
import com.oas.model.I18nMessage;

@Service
public class MessageServiceImpl extends AbstractCrudService<I18nMessage, Long, I18nRepository, MessageLanguageDTO>implements MessageService{

	public MessageServiceImpl(I18nRepository repository) {
		super(repository);
	}

	@Override
	@Transactional(readOnly = false)
	public void createMessageKey(List<KeyValuePairDTO<String, MessageLanguageDTO>> mgs) {
		List<I18nMessage> i18nMessages = mgs.stream()
				.map(I18nMessageMapper::dtoToEntity)
				.collect(Collectors.toList());
		repository.saveAll(i18nMessages);
	}

}
