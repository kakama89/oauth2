package com.oas.business.admin.account;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.oas.dto.AuditDTO;

@Getter
@Setter
public class AccountDTO extends AuditDTO<String>{
	private String id;
	private String username;
	private String status;
	private List<String> roles;
	private String rawPw;
}
