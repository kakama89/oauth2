package com.oas.business.admin.account;

import java.util.stream.Collectors;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.oas.business.admin.authority.AuthorityRepository;
import com.oas.model.Authority;
import com.oas.model.User;
import com.oas.model.User.UserStatus;

public class AccountMapper {
	public static User dtoToEntity(AccountDTO payload, PasswordEncoder encoder, AuthorityRepository authorityRepo){
		User user = new User();
		user.setId(payload.getId());
		user.setUsername(payload.getUsername());
		user.setStatus(UserStatus.valueOf(payload.getStatus()));
		user.setPassword(encoder.encode(payload.getRawPw()));
		user.setRoles(authorityRepo.findByCodeIn(payload.getRoles()));
		return user;
	}
	
	public static AccountDTO entityToDto(User entity){
		AccountDTO account = new AccountDTO();
		account.setId(entity.getId());
		account.setUsername(entity.getUsername());
		account.setStatus(entity.getStatus().name());
		account.setRoles(entity.getRoles().stream().map(Authority::getCode).collect(Collectors.toList()));
		account.setCreatedDate(entity.getCreatedDate());
		account.setCreatedBy(entity.getCreatedBy());
		account.setLastModifiedDate(entity.getLastModifiedDate());
		account.setLastModifiedBy(entity.getLastModifiedBy());
		return account;
	}
}
