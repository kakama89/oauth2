package com.oas.business.admin.account;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface AccountService {
	Page<AccountDTO> findAccounts(Pageable pageable);
	AccountDTO updateAccount(String id, AccountDTO payload);
	AccountDTO findAccount(String id);
	AccountDTO createAccount(AccountDTO payload);
}
