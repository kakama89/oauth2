package com.oas.business.admin.account;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oas.business.AbstractCrudService;
import com.oas.business.admin.authority.AuthorityRepository;
import com.oas.business.user.account.UserRepository;
import com.oas.exception.BusinessException;
import com.oas.exception.ErrorCode;
import com.oas.exception.NotFoundException;
import com.oas.model.User;
import com.oas.model.User.UserStatus;

@Service
public class AccountServiceImpl extends AbstractCrudService<User, String, UserRepository, AccountDTO> implements AccountService{
	private final AuthorityRepository authorityRepository;
	private final PasswordEncoder passwordEncoder;
	public AccountServiceImpl(UserRepository repository, AuthorityRepository authorityRepository, PasswordEncoder passwordEncoder) {
		super(repository);
		this.authorityRepository = authorityRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<AccountDTO> findAccounts(Pageable pageable) {
		Page<User> accounts = repository.findAll(pageable);
		return accounts.map(AccountMapper::entityToDto);
	}
	
	
	
	@Override
	@Transactional(readOnly = false)
	public AccountDTO updateAccount(String id, AccountDTO payload){
		if(repository.existsByUsernameAndIdNot(payload.getUsername(), id)){
			throw new BusinessException(ErrorCode.ERR_00002);
		}
		User user = get(id).orElseThrow(()-> new BusinessException(null));
		user.setUsername(payload.getUsername());
		user.setStatus(UserStatus.valueOf(payload.getStatus()));
		user.setRoles(authorityRepository.findByCodeIn(payload.getRoles()));
		user.setPassword(passwordEncoder.encode(payload.getRawPw()));
		repository.save(user);
		return AccountMapper.entityToDto(user);
	}

	@Override
	@Transactional(readOnly = false)
	public AccountDTO createAccount(AccountDTO payload) {
		if(repository.existsByUsername(payload.getUsername())){
			throw new BusinessException(ErrorCode.ERR_00002);
		}
		User user = new User();
		user.setUsername(payload.getUsername());
		user.setStatus(UserStatus.ACTIVE);
		String password = passwordEncoder.encode(payload.getRawPw());
		user.setPassword(password);
		user.setRoles(authorityRepository.findByCodeIn(payload.getRoles()));
		User createdUser = repository.save(user);
		return AccountMapper.entityToDto(createdUser);
	}

	@Override
	@Transactional(readOnly = true)
	public AccountDTO findAccount(String id) {
		User user = get(id).orElseThrow(()-> new NotFoundException((ErrorCode.ERR_00001)));
		return AccountMapper.entityToDto(user);
	}

}
