package com.oas.business.admin.account;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oas.web.WebUtil;

@RestController
@RequestMapping("/admin/accounts")
public class AccountController {
	private final AccountService accountService;
	public AccountController(final AccountService accountService){
		this.accountService = accountService;
	}
	
	@GetMapping
	public ResponseEntity<Page<AccountDTO>> getAccounts(Pageable pageable){
		Page<AccountDTO> response = accountService.findAccounts(pageable);
		return WebUtil.wrapOrNotFound(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<AccountDTO> getAccount(@PathVariable String id){
		AccountDTO foundAccount = accountService.findAccount(id);
		return WebUtil.wrapOrNotFound(foundAccount);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<AccountDTO> updateAccount(@PathVariable String id, @RequestBody AccountDTO payload){
		AccountDTO updatedAccount = accountService.updateAccount(id, payload);
		return WebUtil.wrapOrNotFound(updatedAccount);
	}
	
	@PostMapping
	public ResponseEntity<AccountDTO> createAccount(@RequestBody AccountDTO payload){
		AccountDTO createdAccount = accountService.createAccount(payload);
		return WebUtil.wrapOrNotFound(createdAccount);
	}
	
}
