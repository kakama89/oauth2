package com.oas.business.admin.authority;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.oas.model.Authority;

@RestController
@RequestMapping("/admin/authorities")
public class AuthorityController {
	public final AuthorityService authorityService;
	private ModelMapper mapper = new ModelMapper();
	public AuthorityController(AuthorityService authorityService) {
		this.authorityService = authorityService;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<AuthorityDTO> getAuthority(@PathVariable Long id){
		AuthorityDTO authority = authorityService.get(id, entity -> mapper.map(entity, AuthorityDTO.class))
				.orElseThrow(NullPointerException::new);
		return ResponseEntity.ok().body(authority);
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<Void> createAuthority(@Valid @RequestBody AuthorityDTO payload) {
		authorityService.create(payload, authorityDTO -> mapper.map(authorityDTO, Authority.class));
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Void> updateAuthority(@PathVariable Long id, @Valid @RequestBody AuthorityDTO payload){
		authorityService.update(id, payload, (dto, entity)->{
			entity.setName(dto.getName());
			entity.setCode(dto.getCode());
		});
		return ResponseEntity.noContent().build();
	}
}
