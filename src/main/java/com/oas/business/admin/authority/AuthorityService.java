package com.oas.business.admin.authority;

import com.oas.business.CrudService;
import com.oas.model.Authority;

public interface AuthorityService extends CrudService<Authority , Long, AuthorityDTO> {

}
