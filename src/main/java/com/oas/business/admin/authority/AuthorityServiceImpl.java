package com.oas.business.admin.authority;

import org.springframework.stereotype.Service;

import com.oas.business.AbstractCrudService;
import com.oas.model.Authority;

@Service
public class AuthorityServiceImpl extends AbstractCrudService<Authority, Long, AuthorityRepository, AuthorityDTO> implements AuthorityService {
	public AuthorityServiceImpl(AuthorityRepository repository) {
		super(repository);
	}

}
