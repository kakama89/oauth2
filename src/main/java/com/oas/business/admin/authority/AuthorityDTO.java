package com.oas.business.admin.authority;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthorityDTO implements Serializable {
	private static final long serialVersionUID = -5909668524965938430L;
	
	public AuthorityDTO(String code, String name) {
		this.name = name;
		this.code = code;
	}
	private Long id;
	@NotBlank
	private String code;
	private String name;
}
