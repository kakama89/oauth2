package com.oas.business.admin.authority;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oas.model.Authority;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long>{
	Set<Authority> findByCodeIn(List<String> codes);
}
