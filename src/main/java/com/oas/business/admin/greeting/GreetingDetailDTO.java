package com.oas.business.admin.greeting;

import com.oas.business.admin.greetingpage.GreetingPageDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class GreetingDetailDTO extends GreetingDTO{
    private List<GreetingPageDTO> pages;
}
