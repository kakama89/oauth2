package com.oas.business.admin.greeting;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oas.web.WebUtil;

@RestController
@RequestMapping("/admin/greetings")
public class GreetingAdminController {
	private final GreetingServiceAdmin greetingServiceAdmin;
	
	@Autowired
	public GreetingAdminController(GreetingServiceAdmin greetingServiceAdmin) {
		this.greetingServiceAdmin = greetingServiceAdmin;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<GreetingDTO> getGreeting(@PathVariable String id){
		GreetingDTO greetingFound = greetingServiceAdmin.findById(id).orElse(null);
		return WebUtil.wrapOrNotFound(greetingFound);
	}
	
	@GetMapping
	public ResponseEntity<Page<GreetingDTO>> getGreetings(Pageable pageable){
		Page<GreetingDTO> greetings = greetingServiceAdmin.findAll(pageable);
		return WebUtil.wrapOrNotFound(greetings);
	}
	
	@GetMapping("/categories/{id}")
	public ResponseEntity<List<GreetingDTO>> getGreetingsByCategory(@PathVariable Long categoryId){
		List<GreetingDTO> greetingDtos = greetingServiceAdmin.findByCategory(categoryId);
		return WebUtil.wrapOrNotFound(greetingDtos);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<GreetingDTO> updateGreeting(@PathVariable String id, @Valid @RequestBody GreetingDTO payload){
		GreetingDTO updatedGreeting = greetingServiceAdmin.updateGreeting(id, payload);
		return WebUtil.wrapOrNotFound(updatedGreeting);
	}
	
	@PostMapping
	public ResponseEntity<GreetingDTO> createGreeting(@Valid @RequestBody GreetingDTO payload){
		GreetingDTO createdGreeting = greetingServiceAdmin.createGreeting(payload);
		return WebUtil.wrapOrNotFound(createdGreeting);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteGreeting(@PathVariable String id){
		greetingServiceAdmin.delete(id);
		return ResponseEntity.ok().build();
	}
}
