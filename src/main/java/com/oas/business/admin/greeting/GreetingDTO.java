package com.oas.business.admin.greeting;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GreetingDTO {
	private String id;
	
	@NotNull
	private String title;
	
	private String thumbnail;
	
	private String status;
	
	private Long categoryId;

	@JsonProperty("likes")
	private Long numberOfLike = 0L;
	
	@JsonProperty("views")
	private Long numberOfView = 0L;
	
	@JsonProperty("picks")
	private Long numberOfPick = 0L;
	
	@JsonProperty("downloadAble")
	private boolean downloadAble;
	
	@JsonProperty("printAble")
	private boolean printAble;
	
	@JsonProperty("sendOnline")
	private boolean sendOnline;
}
