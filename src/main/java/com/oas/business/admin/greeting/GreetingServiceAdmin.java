package com.oas.business.admin.greeting;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.oas.business.CrudService;
import com.oas.model.Greeting;

public interface GreetingServiceAdmin extends CrudService<Greeting, String, GreetingDTO>{
	List<GreetingDTO> findByCategory(Long categoryId);
	Page<GreetingDTO> findAll(Pageable pageable);
	Optional<GreetingDTO> findById(String id);
	GreetingDTO createGreeting(GreetingDTO payload);
	GreetingDTO updateGreeting(String id, GreetingDTO payload);	
}

