package com.oas.business.admin.greeting;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.oas.business.admin.category.CategoryRepository;
import com.oas.business.admin.greetingpage.GreetingPageDTO;
import com.oas.business.admin.greetingpage.GreetingPageMapper;
import com.oas.model.Greeting;
import com.oas.model.Greeting.Status;
import com.oas.model.GreetingCategory;

@Component
public class GreetingMapper {
	private final GreetingRepository greetingRepo;
	private final CategoryRepository categoryRepo;
	private final GreetingPageMapper greetingPageMapper;
	
	public GreetingMapper(final GreetingRepository greetingRepo,
			CategoryRepository categoryRepo,
			GreetingPageMapper greetingPageMapper) {
		this.greetingRepo = greetingRepo;
		this.categoryRepo = categoryRepo;
		this.greetingPageMapper = greetingPageMapper;
	}

	public Greeting toEntity(String id, GreetingDTO payload) {
		Greeting greeting = Optional.ofNullable(id)
				.flatMap(greetingRepo::findById)
				.orElseGet(Greeting::new);
		greeting.setTitle(payload.getTitle());
		greeting.setThumbnail(payload.getThumbnail());
		Status status = id == null ? Status.NEW : Status.valueOf(payload.getStatus());
		greeting.setStatus(status);
		GreetingCategory category = Optional.ofNullable(payload.getCategoryId())
				.flatMap(categoryRepo::findById).orElse(null);
		greeting.setCategory(category);
		greeting.setDownloadAble(payload.isDownloadAble());
		greeting.setPrintAble(payload.isPrintAble());
		greeting.setSendOnline(payload.isSendOnline());
		return greeting;
	}
	
	public GreetingDetailDTO fromEntity(Greeting entity) {
		GreetingDetailDTO result = new GreetingDetailDTO();
		result.setId(entity.getId());
		result.setTitle(entity.getTitle());
		result.setThumbnail(entity.getThumbnail());
		result.setStatus(entity.getStatus().name());
		result.setCategoryId(entity.getCategory().getId());
		result.setDownloadAble(entity.isDownloadAble());
		result.setPrintAble(entity.isPrintAble());
		result.setSendOnline(entity.isSendOnline());
		List<GreetingPageDTO> pages = entity.getPages().parallelStream()
				.map(greetingPageMapper::fromEntity)
				.collect(Collectors.toList());
		result.setPages(pages);
		return result;
	}
}