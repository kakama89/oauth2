package com.oas.business.admin.greeting;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oas.model.Greeting;


@Repository
public interface GreetingRepository extends JpaRepository<Greeting, String>{
	@Query("select g from Greeting g inner join g.category c where c.id = :id")
	List<Greeting> findByCategoryId(@Param("id") Long id);
}
