package com.oas.business.admin.greeting;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oas.business.AbstractCrudService;
import com.oas.model.Greeting;

@Service
public class GreetingServiceAdminImpl extends AbstractCrudService<Greeting, String, GreetingRepository, GreetingDTO>implements GreetingServiceAdmin{
	private final GreetingMapper mapper;
	
	public GreetingServiceAdminImpl(GreetingRepository repository, GreetingMapper mapper) {
		super(repository);
		this.mapper = mapper;
	}

	@Override
	@Transactional(readOnly = true)
	public List<GreetingDTO> findByCategory(Long categoryId) {
		return repository.findByCategoryId(categoryId)
				.stream()
				.map(mapper::fromEntity)
				.collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<GreetingDTO> findById(String id) {
		return get(id).map(mapper::fromEntity);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GreetingDTO> findAll(Pageable pageable) {
		return repository.findAll(pageable).map(mapper::fromEntity);
	}

	@Override
	@Transactional(readOnly = false)
	public GreetingDTO createGreeting(GreetingDTO payload) {
		Greeting entity =  mapper.toEntity(null, payload);
		repository.save(entity);
		return mapper.fromEntity(entity);
	}

	@Override
	@Transactional(readOnly = false)
	public GreetingDTO updateGreeting(String id, GreetingDTO payload) {
		Greeting entity =  mapper.toEntity(id, payload);
		repository.save(entity);
		return mapper.fromEntity(entity);
	}
}
