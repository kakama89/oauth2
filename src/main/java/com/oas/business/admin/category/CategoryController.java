package com.oas.business.admin.category;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oas.web.WebUtil;

@RestController
@RequestMapping("/admin/categories")
public class CategoryController {
	
	private final CategoryService gCategoryServiceAdmin;
	
	@Autowired
	public CategoryController(CategoryService gCategoryServiceAdmin){
		this.gCategoryServiceAdmin = gCategoryServiceAdmin;
	}
	
	@GetMapping
	public ResponseEntity<Page<CategoryDTO>> getGreetingCategories(Pageable pageable){
		Page<CategoryDTO> categories = gCategoryServiceAdmin.findAll(pageable);
		return WebUtil.wrapOrNotFound(categories);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CategoryDTO> getGreetingCategory(@PathVariable Long id){
		CategoryDTO foundGreeting = gCategoryServiceAdmin.getGreetingCategory(id);
		return WebUtil.wrapOrNotFound(foundGreeting);
	}
	
	@PostMapping
	public ResponseEntity<CategoryDTO> createGreetingCategory(@Valid @RequestBody CategoryDTO payload){
		CategoryDTO createdGreeting = gCategoryServiceAdmin.createGreetingCategory(payload);
		return WebUtil.wrapOrNotFound(createdGreeting);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<CategoryDTO> updateGreetingCategory(@PathVariable Long id, @Valid @RequestBody CategoryDTO payload){
		CategoryDTO updatedGreeting = gCategoryServiceAdmin.updateGreetingCategory(id, payload);
		return WebUtil.wrapOrNotFound(updatedGreeting);
	}
}
