package com.oas.business.admin.category;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.oas.business.admin.category.CategoryDTO;

public interface CategoryService{
	CategoryDTO getGreetingCategory(Long pk);
	CategoryDTO createGreetingCategory(CategoryDTO payload);
	CategoryDTO updateGreetingCategory(Long id, CategoryDTO payload);
	Page<CategoryDTO> findAll(Pageable pageable);
}

