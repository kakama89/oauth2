package com.oas.business.admin.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oas.model.GreetingCategory;

@Repository
public interface CategoryRepository extends JpaRepository<GreetingCategory, Long> {

}
