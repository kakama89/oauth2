package com.oas.business.admin.category;

import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oas.business.AbstractCrudService;
import com.oas.exception.ErrorCode;
import com.oas.exception.NotFoundException;
import com.oas.model.GreetingCategory;

@Service
public class CategoryServiceImpl extends AbstractCrudService<GreetingCategory, Long, CategoryRepository, CategoryDTO> implements CategoryService{
	public CategoryServiceImpl(CategoryRepository repository) {
		super(repository);
	}

	
	@Override
	@Transactional(readOnly = true)
	public CategoryDTO getGreetingCategory(Long pk) {
		GreetingCategory entity = get(pk).orElseThrow(()->new NotFoundException(ErrorCode.ERR_00001));
		Hibernate.initialize(entity.getSubCategories());
		return CategoryMapper.entityToDTO(entity, true);
	}



	@Override
	@Transactional(readOnly = false)
	public CategoryDTO createGreetingCategory(CategoryDTO payload) {
		GreetingCategory category = CategoryMapper.dtoToEntity(payload);
		if(payload.getParentId() != null){
			GreetingCategory parent = get(payload.getParentId()).orElse(null);
			category.setParent(parent);
		}
		repository.save(category);
		return CategoryMapper.entityToDTO(category, false);
	}


	@Override
	@Transactional(readOnly = false)
	public CategoryDTO updateGreetingCategory(Long id, CategoryDTO payload) {
		GreetingCategory category = get(id).orElseThrow(()-> new NotFoundException(ErrorCode.ERR_00001));
		category = CategoryMapper.mergeToEntity(payload, category);
		repository.save(category);
		return CategoryMapper.entityToDTO(category, false);
	}


	@Override
	@Transactional(readOnly = true)
	public Page<CategoryDTO> findAll(Pageable pageable) {
		return repository.findAll(pageable).map(category -> CategoryMapper.entityToDTO(category, true));
	}
}
