package com.oas.business.admin.category;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Setter
@NoArgsConstructor
public class CategoryDTO {
	
	private Long id;
	
	@NotNull
	private String code;
	
	@NotNull
	private String name;
	
	private Long priority;
	
	private Long parentId;
	
	private Long version;
	
	@JsonInclude(value = Include.NON_NULL)
	private List<CategoryDTO> children;
}
