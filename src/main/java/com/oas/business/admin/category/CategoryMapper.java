package com.oas.business.admin.category;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.oas.model.GreetingCategory;

public class CategoryMapper {
	private CategoryMapper(){}
	public static CategoryDTO entityToDTO(GreetingCategory category, boolean fetchSubCategory){
		CategoryDTO dto = new CategoryDTO();
		dto.setId(category.getId());
		dto.setName(category.getName());
		dto.setCode(category.getCode());
		dto.setPriority(category.getPriority());
		dto.setParentId(Optional.ofNullable(category.getParent()).map(GreetingCategory::getId).orElse(null));
		if(fetchSubCategory){
			List<CategoryDTO> subCategories = category.getSubCategories()
					.stream()
					.map(e -> CategoryMapper.entityToDTO(e, fetchSubCategory))
					.collect(Collectors.toList());
			dto.setChildren(subCategories);
		}
		return dto;
	}
	public static GreetingCategory dtoToEntity(CategoryDTO payload){
		GreetingCategory category = new GreetingCategory();
		category.setId(payload.getId());
		return mergeToEntity(payload, category);
	}
	
	public static GreetingCategory mergeToEntity(CategoryDTO payload, GreetingCategory category){
		category.setName(payload.getName());
		category.setCode(payload.getCode());
		category.setPriority(payload.getPriority());
		return category;
	}
}
