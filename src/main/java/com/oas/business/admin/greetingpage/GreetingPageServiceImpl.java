package com.oas.business.admin.greetingpage;

import com.oas.business.admin.greeting.GreetingRepository;
import com.oas.exception.ErrorCode;
import com.oas.exception.NotFoundException;
import com.oas.model.Greeting;
import com.oas.model.GreetingPage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GreetingPageServiceImpl implements GreetingPageService{

    private final GreetingRepository repository;
    private final GreetingPageRepository pageRepository;
    private final GreetingPageMapper mapper;

    public GreetingPageServiceImpl(GreetingRepository repository,
                                   GreetingPageRepository pageRepository,
                                   GreetingPageMapper mapper){
        this.repository = repository;
        this.pageRepository = pageRepository;
        this.mapper = mapper;
    }

    @Override
    @Transactional(readOnly = false)
    public GreetingPageDTO createPage(String greetingId, GreetingPageDTO payload) {
        Greeting greeting = repository.findById(greetingId).orElseThrow(() -> new NotFoundException(ErrorCode.ERR_00001));
        GreetingPage page = mapper.toEntity(null, payload);
        page.setGreeting(greeting);
        GreetingPage createdPage = pageRepository.save(page);
        greeting.getPages().add(createdPage);
        repository.save(greeting);
        return mapper.fromEntity(createdPage);
    }

    @Override
    @Transactional(readOnly = false)
    public GreetingPageDTO updatePage(Long pageId, GreetingPageDTO payload) {
        GreetingPage page = mapper.toEntity(pageId, payload);
        GreetingPage updatedPage = pageRepository.save(page);
        return mapper.fromEntity(updatedPage);
    }

    @Override
    @Transactional(readOnly = true)
    public GreetingPageDTO getPage(Long pageId) {
        GreetingPage page = pageRepository.findById(pageId).orElseThrow(() -> new NotFoundException(ErrorCode.ERR_00001));
        return mapper.fromEntity(page);
    }

}
