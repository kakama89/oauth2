package com.oas.business.admin.greetingpage;

import java.util.List;

import com.oas.business.admin.media.image.MediaImageDTO;
import com.oas.business.admin.media.text.MediaTextDTO;
import com.oas.dto.HTMLAtrributedDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GreetingPageDTO extends HTMLAtrributedDTO{
	private Long id;
	private String greetingId;
	private Integer page;
	private String bgImage;
	private String bgColor;
	private List<MediaTextDTO> texts;
	private List<MediaImageDTO> images;
}
