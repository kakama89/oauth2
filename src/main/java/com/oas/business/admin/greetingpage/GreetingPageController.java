package com.oas.business.admin.greetingpage;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oas.web.WebUtil;

@RestController
@RequestMapping("/admin/greetings")
public class GreetingPageController {

    private final GreetingPageService pageService;

    public GreetingPageController(GreetingPageService pageService){
        this.pageService = pageService;
    }

    @GetMapping("/{greetingId}/pages")
    public ResponseEntity<List<GreetingPageDTO>> getPages(@PathVariable String greetingId){
        return null;
    }

    @PostMapping("/{greetingId}/pages")
    public ResponseEntity<GreetingPageDTO> createPage(@PathVariable String greetingId, @RequestBody GreetingPageDTO payload){
        GreetingPageDTO createdPage = pageService.createPage(greetingId, payload);
        return WebUtil.wrapOrNotFound(createdPage);
    }

    @GetMapping("/{greetingId}/pages/{pageId}")
    public ResponseEntity<GreetingPageDTO> getPage(@PathVariable String greetingId, @PathVariable Long pageId){
        GreetingPageDTO page = pageService.getPage(pageId);
        return WebUtil.wrapOrNotFound(page);
    }

    @PutMapping("/{greetingId}/pages/{pageId}")
    public ResponseEntity<GreetingPageDTO> updatePage(@PathVariable String greetingId,
                                                      @PathVariable Long pageId,
                                                      @RequestBody GreetingPageDTO payload){
        GreetingPageDTO updatedPage = pageService.updatePage(pageId, payload);
        return WebUtil.wrapOrNotFound(updatedPage);
    }
}
