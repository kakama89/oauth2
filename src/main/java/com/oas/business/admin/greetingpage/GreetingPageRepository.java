package com.oas.business.admin.greetingpage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oas.model.GreetingPage;

@Repository
public interface GreetingPageRepository extends JpaRepository<GreetingPage, Long>{

}
