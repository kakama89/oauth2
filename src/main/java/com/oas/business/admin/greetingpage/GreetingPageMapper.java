package com.oas.business.admin.greetingpage;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.oas.business.admin.media.image.MediaImageDTO;
import com.oas.business.admin.media.image.MediaImageMapper;
import com.oas.business.admin.media.text.MediaTextDTO;
import com.oas.business.admin.media.text.MediaTextMapper;
import com.oas.model.GreetingPage;
import com.oas.model.MediaImage;
import com.oas.model.MediaText;

@Component
public class GreetingPageMapper {
	private final GreetingPageRepository greetingPageRepository;
	private final MediaImageMapper imageMapper;
	private final MediaTextMapper textMapper;
	public GreetingPageMapper(GreetingPageRepository greetingPageRepository,
			MediaImageMapper imageMapper, MediaTextMapper textMapper) {
		this.greetingPageRepository = greetingPageRepository;
		this.imageMapper = imageMapper;
		this.textMapper = textMapper;
	}
	
	public GreetingPageDTO fromEntity(GreetingPage entity) {
		GreetingPageDTO result = new GreetingPageDTO();
		result.setId(entity.getId());
		result.setBgColor(entity.getBackgroundColor());
		result.setBgImage(entity.getBackgroundImage());
		result.setPage(entity.getPage());
		result.setWidth(entity.getWidth());
		result.setHeight(entity.getHeight());
		result.setTop(entity.getTop());
		result.setLeft(entity.getLeft());
		result.setStyle(entity.getStyle());
		result.setGreetingId(entity.getGreeting().getId());
		List<MediaImageDTO> images = entity.getMediaImages().parallelStream()
			.map(imageMapper::fromEntity)
			.collect(Collectors.toList());
		result.setImages(images);
		
		List<MediaTextDTO> texts = entity.getMediaTexts().parallelStream()
				.map(textMapper::fromEntity)
				.collect(Collectors.toList());
		result.setTexts(texts);
		return result;
		
	}
	
	
	
	public GreetingPage toEntity(Long id, GreetingPageDTO payload) {
		GreetingPage entity = Optional.ofNullable(id)
				.flatMap(greetingPageRepository::findById).
				orElseGet(GreetingPage::new);
		entity.setBackgroundImage(payload.getBgImage());
		entity.setBackgroundColor(payload.getBgColor());
		entity.setWidth(payload.getWidth());
		entity.setHeight(payload.getHeight());
		entity.setTop(payload.getTop());
		entity.setLeft(payload.getLeft());
		entity.setPage(payload.getPage());
		// convert image
		List<MediaImageDTO> imgs = payload.getImages();
		if(imgs == null) {
			imgs = Collections.emptyList();
		}
		List<MediaImage> images = imgs.stream()
		.map(e ->{
			MediaImage image = imageMapper.toEntity(e.getId(), e);
			image.setPage(entity);
			return image;
		})
		.collect(Collectors.toList());
		entity.getMediaImages().clear();
		entity.getMediaImages().addAll(images);
		
		// convert text
		List<MediaTextDTO> txts = payload.getTexts();
		if(txts == null) {
			txts = Collections.emptyList();
		}
		
		List<MediaText> texts = txts.stream()
				.map(e ->{
					MediaText txt = textMapper.toEntity(e.getId(), e);
					txt.setPage(entity);
					return txt;
				})
				.collect(Collectors.toList());
		entity.getMediaTexts().clear();
		entity.getMediaTexts().addAll(texts);
		return entity;
	}
}
