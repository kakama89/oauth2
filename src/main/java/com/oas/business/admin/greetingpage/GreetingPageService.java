package com.oas.business.admin.greetingpage;

public interface GreetingPageService {
    GreetingPageDTO createPage(String greetingId, GreetingPageDTO payload);
    GreetingPageDTO updatePage(Long pageId, GreetingPageDTO payload);
    GreetingPageDTO getPage(Long pageId);
}
