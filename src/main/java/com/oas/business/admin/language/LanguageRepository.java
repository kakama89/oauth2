package com.oas.business.admin.language;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oas.model.Language;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Long> {
	List<Language>findAllByOrderByCode();
}
