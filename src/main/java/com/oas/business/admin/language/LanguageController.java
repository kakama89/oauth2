package com.oas.business.admin.language;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oas.web.WebUtil;

@RestController
@RequestMapping("/admin/languages")
public class LanguageController {
	private final LanguageService languageService;
	public LanguageController(LanguageService languageService){
		this.languageService = languageService;
	}
	
	@GetMapping
	public ResponseEntity<List<LanguageDTO>> getLanguages(){
		List<LanguageDTO> languages = languageService.findAllOrderByCode();
		return WebUtil.wrapOrNotFound(languages);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<LanguageDTO> getLanguage(@PathVariable Long id){
		return WebUtil.wrapOrNotFound(languageService.getLanguage(id));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<LanguageDTO> updateLanguage(@PathVariable Long id , @RequestBody LanguageDTO payload){
		return WebUtil.wrapOrNotFound(languageService.updateLanguage(id, payload));
	}
	
	@PostMapping
	public ResponseEntity<LanguageDTO> addLanguage(@RequestBody LanguageDTO payload){
		return WebUtil.wrapOrNotFound(languageService.createLanguage(payload));
	}
}
