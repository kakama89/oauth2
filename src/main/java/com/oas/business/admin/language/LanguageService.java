package com.oas.business.admin.language;

import java.util.List;

import com.oas.business.CrudService;
import com.oas.model.Language;

public interface LanguageService  extends CrudService<Language, Long, LanguageDTO>{
	List<LanguageDTO>findAllOrderByCode();
	LanguageDTO getLanguage(Long id);
	LanguageDTO updateLanguage(Long id, LanguageDTO payload);
	LanguageDTO createLanguage(LanguageDTO payload);
}
