package com.oas.business.admin.language;

import com.oas.model.Language;
import com.oas.model.Language.Status;

public class LanguageMapper {
	public static Language dtoToEntity(LanguageDTO payload) {
		Language entity = new Language();
		entity.setCode(payload.getCode());
		entity.setName(payload.getName());
		entity.setNameEng(payload.getNameEng());
		entity.setStatus(Language.Status.valueOf(payload.getStatus()));
		return entity;
	}
	
	public static LanguageDTO entityToDto(Language entity){
		LanguageDTO dto = new LanguageDTO();
		dto.setCode(entity.getCode());
		dto.setName(entity.getName());
		dto.setNameEng(entity.getNameEng());
		dto.setStatus(entity.getStatus().name());
		return dto;
	}
	
	public static Language mergeToEntity(LanguageDTO payload, Language lang){
		lang.setCode(payload.getCode());
		lang.setName(payload.getName());
		lang.setNameEng(payload.getNameEng());
		lang.setStatus(Status.valueOf(payload.getStatus()));
		return lang;
	}
}
