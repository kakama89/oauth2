package com.oas.business.admin.language;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageDTO {
	private Long id;
	
	private String name;
	private String nameEng;
	private String code;
	private String status;
}
