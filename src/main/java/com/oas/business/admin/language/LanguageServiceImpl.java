package com.oas.business.admin.language;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oas.business.AbstractCrudService;
import com.oas.exception.ErrorCode;
import com.oas.exception.NotFoundException;
import com.oas.model.Language;

@Service
public class LanguageServiceImpl extends AbstractCrudService<Language, Long, LanguageRepository, LanguageDTO>
		implements LanguageService {

	public LanguageServiceImpl(LanguageRepository repository) {
		super(repository);
	}

	@Override
	@Transactional(readOnly = true)
	public List<LanguageDTO> findAllOrderByCode() {
		List<Language> languages = repository.findAllByOrderByCode();
		return languages.stream().map(LanguageMapper::entityToDto)
				.collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public LanguageDTO getLanguage(Long id) {
		return get(id).map(LanguageMapper::entityToDto).orElseThrow(()-> new NotFoundException(ErrorCode.ERR_00001));
	}

	@Override
	@Transactional(readOnly = false)
	public LanguageDTO updateLanguage(Long id, LanguageDTO payload) {
		Language lang = get(id).orElseThrow(()-> new NotFoundException(ErrorCode.ERR_00001));
		lang = LanguageMapper.mergeToEntity(payload, lang);
		repository.save(lang);
		return LanguageMapper.entityToDto(lang);
	}

	@Override
	@Transactional(readOnly = false)
	public LanguageDTO createLanguage(LanguageDTO payload) {
		Language lang = LanguageMapper.dtoToEntity(payload) ;
		repository.save(lang);
		return LanguageMapper.entityToDto(lang);
	}


	
}
