package com.oas.business.admin.media;

import com.oas.dto.HTMLAtrributedDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaDTO extends HTMLAtrributedDTO {
	private Long id;
	private String type;	
}
