package com.oas.business.admin.media.image;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.oas.model.MediaImage;

@Component
public class MediaImageMapper {
	private final MediaImageRepository mediaImageRepository;
	public MediaImageMapper(MediaImageRepository mediaImageRepository) {
		this.mediaImageRepository = mediaImageRepository;
	}
	public MediaImageDTO fromEntity(MediaImage entity) {
		MediaImageDTO result = new MediaImageDTO();
		result.setId(entity.getId());
		result.setBackground(entity.getBackground());
		result.setWidth(entity.getWidth());
		result.setHeight(entity.getHeight());
		result.setTop(entity.getTop());
		result.setLeft(entity.getLeft());
		result.setStyle(entity.getStyle());
		result.setType("image");
		return result;
	}
	
	public MediaImage toEntity(Long id, MediaImageDTO payload) {
		MediaImage entity = Optional.ofNullable(id)
				.flatMap(mediaImageRepository::findById)
				.orElseGet(MediaImage::new);
		return map(payload, entity);
	}
	
	private MediaImage map(MediaImageDTO payload, MediaImage entity) {
		
		entity.setBackground(payload.getBackground());
		entity.setWidth(payload.getWidth());
		entity.setHeight(payload.getHeight());
		entity.setTop(payload.getTop());
		entity.setLeft(payload.getLeft());
		entity.setStyle(payload.getStyle());
		return entity;
	}
}

