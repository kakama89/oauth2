package com.oas.business.admin.media.image;

import com.oas.business.admin.media.MediaDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaImageDTO extends MediaDTO {
	private String background;
}
