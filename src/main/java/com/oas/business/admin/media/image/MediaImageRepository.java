package com.oas.business.admin.media.image;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oas.model.MediaImage;

@Repository
public interface MediaImageRepository extends JpaRepository<MediaImage, Long> {

}
