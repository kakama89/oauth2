package com.oas.business.admin.media.text;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.oas.model.MediaText;

@Component
public class MediaTextMapper {
	private final MediaTextRepository mediaTextRepository;

	public MediaTextMapper(MediaTextRepository mediaTextRepository) {
		this.mediaTextRepository = mediaTextRepository;
	}
	public MediaTextDTO fromEntity(MediaText entity) {
		MediaTextDTO result = new MediaTextDTO();
		result.setId(entity.getId());
		result.setText(entity.getText());
		result.setWidth(entity.getWidth());
		result.setHeight(entity.getHeight());
		result.setTop(entity.getTop());
		result.setLeft(entity.getLeft());
		result.setStyle(entity.getStyle());
		result.setType("text");
		return result;
	}
	public MediaText toEntity(Long id, MediaTextDTO payload) {
		MediaText entity = Optional.ofNullable(id)
				.flatMap(mediaTextRepository::findById)
				.orElseGet(MediaText::new);
		return map(payload, entity);
	}
	
	private MediaText map(MediaTextDTO payload, MediaText entity) {
		entity.setText(payload.getText());
		entity.setWidth(payload.getWidth());
		entity.setHeight(payload.getHeight());
		entity.setTop(payload.getTop());
		entity.setLeft(payload.getLeft());
		entity.setStyle(payload.getStyle());
		return entity;
	}
}
