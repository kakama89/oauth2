package com.oas.business.admin.media.text;

import com.oas.business.admin.media.MediaDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaTextDTO extends MediaDTO{
	private String text;
}
