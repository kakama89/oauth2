package com.oas.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HTMLAtrributedDTO {
	private float width;
	private float height;
	private float top;
	private float left;
	private String style;
}
