package com.oas.dto;

import java.time.Instant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuditDTO<T> {
	private T createdBy;

	private Instant createdDate;

	private T lastModifiedBy;

	private Instant lastModifiedDate;
}
