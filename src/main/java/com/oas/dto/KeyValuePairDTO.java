package com.oas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyValuePairDTO<K, V>{
	
	@JsonInclude(value = Include.NON_NULL)
	private K key;

	@JsonInclude(value = Include.NON_NULL)
	private V value;
}
