package com.oas.system.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.oas.model.User;
import com.oas.system.config.security.SecurityUtil;
/**
 * Spring autitor configuration
 * @author oak
 *
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String>{
	@Override
	public Optional<String> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication == null || !authentication.isAuthenticated()){
			return Optional.empty();
		}
		User user = SecurityUtil.getActualUser();
		return Optional.of(user.getId());
	}
}
