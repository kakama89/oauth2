package com.oas.system.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import liquibase.integration.spring.SpringLiquibase;

/**
 * Liquibase configuration
 * @author oak
 *
 */
@Configuration
public class LiquibaseConfig {
	
	private final DataSource dataSource;
	public LiquibaseConfig(final DataSource dataSource) {
		this.dataSource = dataSource;
	}
	@Bean
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog("classpath:liquibase/master.xml");
		return liquibase;	
	}
}
