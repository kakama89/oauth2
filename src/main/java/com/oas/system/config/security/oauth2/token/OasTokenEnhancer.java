package com.oas.system.config.security.oauth2.token;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import com.oas.model.User;

public class OasTokenEnhancer implements TokenEnhancer {
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		User principal = (User)authentication.getPrincipal();
		Map<String,Object> additionalInformation = new HashMap<>();
		additionalInformation.put("authorities", principal.getAuthorities());
		((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(additionalInformation);
		return accessToken;
	}

}
