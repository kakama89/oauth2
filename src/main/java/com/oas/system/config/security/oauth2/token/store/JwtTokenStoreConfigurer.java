package com.oas.system.config.security.oauth2.token.store;

import java.security.KeyPair;
import java.util.Arrays;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import com.oas.system.config.security.oauth2.Oauth2Properties;
import com.oas.system.config.security.oauth2.token.OasTokenEnhancer;
import com.oas.system.config.security.oauth2.token.TokenProperties.KeyStoreConfig;

@Configuration
@ConditionalOnProperty(prefix = "oauth2", name = "token.store", havingValue = "jwt", matchIfMissing = true)
public class JwtTokenStoreConfigurer extends AbstractTokenStoreConfigurer{
    
    private final Oauth2Properties oauth2Properties;
    
    public JwtTokenStoreConfigurer(final DataSource dataSource, final Oauth2Properties oauth2Properties){
        super(dataSource);
        this.oauth2Properties = oauth2Properties;
        
    }

    @Bean
    @Override
	public TokenStore tokenStore() {
		JwtTokenStore tokenStore = new JwtTokenStore(jwtAccessTokenConverter());
		tokenStore.setApprovalStore(approvalStore());
		return tokenStore;
    }
    

    @Bean
    @Override
    public TokenEnhancerChain tokenEnhancerChain(){
        final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(new OasTokenEnhancer(), jwtAccessTokenConverter()));
        return tokenEnhancerChain;
    }

    @Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		KeyStoreConfig keyStore = oauth2Properties.getToken().getKeystore();
		ClassPathResource resource = new ClassPathResource(keyStore.getKey());
		KeyStoreKeyFactory factory = new KeyStoreKeyFactory(resource, keyStore.getPassword().toCharArray());
		KeyPair keyPair = factory.getKeyPair(keyStore.getAlias());
		converter.setKeyPair(keyPair);
		return converter;
	}
}