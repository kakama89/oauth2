package com.oas.system.config.security.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Component;

import com.oas.system.config.security.EAuthority;

@Component
@EnableResourceServer
public class RersourceServerConfigurer extends ResourceServerConfigurerAdapter {
	
	private final DefaultTokenServices tokenService;
	
	private final Oauth2Properties oauth2Properties;
	
	@Autowired
	public RersourceServerConfigurer(final DefaultTokenServices tokenService, final Oauth2Properties oauth2Properties) {
		this.tokenService = tokenService;
		this.oauth2Properties = oauth2Properties;
		
	}
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources
		.resourceId(oauth2Properties.getResourceId())
		.tokenServices(tokenService).stateless(true);
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers(HttpMethod.POST, "/u/accounts/auth", "/u/token/refresh").permitAll()
		.antMatchers("/admin/**", "/actuator/**").hasAuthority(EAuthority.ROLE_ADMIN.name())
		.antMatchers("/swagger*/**", "/webjars/**").permitAll();
		super.configure(http);
	}
}
