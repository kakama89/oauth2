package com.oas.system.config.security;

public enum EAuthority {
	ROLE_ADMIN, ROLE_USER
}
