package com.oas.system.config.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.oas.model.User;

public class SecurityUtil {
	private SecurityUtil() {
		throw new AssertionError();
	}
	
	public static User getActualUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth == null || !auth.isAuthenticated()) {
			return null;
		}
		return  (User)auth.getPrincipal();
	}
}
