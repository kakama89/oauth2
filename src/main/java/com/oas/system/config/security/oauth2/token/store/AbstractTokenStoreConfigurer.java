package com.oas.system.config.security.oauth2.token.store;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
public abstract class AbstractTokenStoreConfigurer{
    protected final DataSource dataSource;

    public AbstractTokenStoreConfigurer(final DataSource dataSource){
        this.dataSource = dataSource;
    }

    @Bean
	public ApprovalStore approvalStore(){
		return new JdbcApprovalStore(dataSource);
    }

    public abstract TokenEnhancerChain tokenEnhancerChain();
    public abstract TokenStore tokenStore();
}