package com.oas.system.config.security.oauth2;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.oas.system.config.security.oauth2.token.TokenProperties;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "oauth2")
public class Oauth2Properties {
	@NotNull
	private TokenProperties token;
	
	@NotNull
	private String resourceId;
	
	@NotNull
	private String clientId;
	
	@NotNull
	private String clientSecret;
}
