package com.oas.i18n;

import com.oas.business.admin.message.I18nRepository;
import com.oas.model.I18nMessage;

import org.springframework.context.support.AbstractMessageSource;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Optional;

@Component

public class DataBaseMessageSource extends AbstractMessageSource {
    private final I18nRepository i18nRepository;

    public DataBaseMessageSource(I18nRepository i18nRepository) {
        this.i18nRepository = i18nRepository;
    }

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        Optional<I18nMessage> opt = i18nRepository.findByCodeAndLocale(code, locale.getLanguage());

        if (opt.isPresent()) {
        	return createMessageFormat(opt.get().getMessage(), locale);
        }
        return createMessageFormat("", locale);
    }
    
}
