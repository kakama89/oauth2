package com.oas.i18n;

import java.util.Locale;


public interface I18nService{
    String get(final String message);
    String get(final String message, Locale locale);
    String get(final String message, final Object... objects);
    String get(final String message, Locale locale, final Object... objects);
    
    String getFromDb(final String message);
    String getFromDb(final String message, Locale locale);
    String getFromDb(final String message, final Object... objects);
    String getFromDb(final String message, Locale locale, final Object... objects);
}
