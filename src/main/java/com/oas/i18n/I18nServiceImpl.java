package com.oas.i18n;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class I18nServiceImpl implements I18nService{
	private MessageSource messageSource;
	private MessageSource messageSourceDb;
	
	public I18nServiceImpl(@Qualifier("messageSource") MessageSource messageSource, @Qualifier("dataBaseMessageSource")MessageSource messageSourceDb){
		this.messageSource = messageSource;
		this.messageSourceDb = messageSourceDb;
	}

	
    private String get(MessageSource messageSource, final String message, Locale locale){
		Object[] empty = null;
    	return get(messageSource, message, locale, empty);
    }
    
    private String get(MessageSource messageSource, final String message, Locale locale, Object ...objects){
    	return messageSource.getMessage(message, objects, locale);
    }

	@Override
	public String get(String message, Locale locale) {
		return get(messageSource, message, locale);
	}

	@Override
	public String get(String message, Locale locale, Object... objects) {
		return get(messageSource, message, locale, objects);
	}

	@Override
	public String getFromDb(String message, Locale locate) {
		return get(messageSourceDb, message, locate);
	}

	@Override
	public String getFromDb(String message, Locale locale, Object... objects) {
		return get(messageSourceDb, message, locale, objects);
	}

	@Override
	public String get(String message) {
		return get(message, LocaleContextHolder.getLocale());
	}

	@Override
	public String get(String message, Object... objects) {
		return get(message, LocaleContextHolder.getLocale(), objects);
	}

	@Override
	public String getFromDb(String message) {
		return getFromDb(message, LocaleContextHolder.getLocale());
	}

	@Override
	public String getFromDb(String message, Object... objects) {
		return getFromDb(message, LocaleContextHolder.getLocale(), objects);
	}
}
