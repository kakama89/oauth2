package com.oas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
public class Oauth2SecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Oauth2SecurityApplication.class, args);
	}

}
