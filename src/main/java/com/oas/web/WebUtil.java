package com.oas.web;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


public class WebUtil {
	private WebUtil(){
		throw new IllegalAccessError("not supported");
	}
	public static final String baseUrl() {
		return ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
	}

	public static final BodyInserters.FormInserter<String> body(Map<String, String> params){
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		params.forEach(map::add);
		return BodyInserters.fromFormData(map);
	}
	
	public static final String getBasicAuth(String username, String secret){
		return "Basic " + Base64Utils.encodeToString((String.format("%s:%s", username, secret))
						.getBytes(StandardCharsets.UTF_8));
	}
	
	public static final<T> ResponseEntity<T> wrapOrNotFound(T dto){
		if(dto == null){
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(dto);
	}
}
