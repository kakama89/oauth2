package com.oas.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.oas.exception.AbstractException;
import com.oas.exception.BadRequestException;
import com.oas.exception.BusinessException;
import com.oas.exception.NotFoundException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String error = "Malformed JSON request";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(status, ex, error));
	}

	@ExceptionHandler(BusinessException.class)
	public ResponseEntity<ApiError> handleBusinessException(BusinessException ex) {
		return responseError(ex, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ApiError> handleNotFoundException(NotFoundException ex) {
		return responseError(ex, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<ApiError> handleBadRequestException(NotFoundException ex) {
		return responseError(ex, HttpStatus.BAD_REQUEST);
	}
	
	private ResponseEntity<ApiError> responseError(AbstractException ex, HttpStatus status){
		return ResponseEntity.status(status).body(new ApiError(status, ex, ex.getErrorCode().name()));
	}
	
	
}
