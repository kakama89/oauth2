package com.oas.util;

import org.codehaus.jackson.map.ObjectMapper;

public class TestUtil {
	private TestUtil() {

	}

	public static String writeAsString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
