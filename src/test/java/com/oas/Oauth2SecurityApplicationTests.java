package com.oas;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;


@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class Oauth2SecurityApplicationTests {

	@ParameterizedTest
	@ValueSource(strings = {""})
	public void testJunit5(String str) {
		assertTrue(StringUtils.isEmpty(str));	
	}

}
