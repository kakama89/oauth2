package com.oas.business.admin.authority;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.oas.util.TestUtil;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AuthorityServiceImplTest {
	@Autowired
	private MockMvc mocMvc;

	@Test
	@WithMockUser(username = "admin", authorities = "ROLE_ADMIN")
	public void testCreateAuthoritySuccess() throws Exception {
		AuthorityDTO payload = new AuthorityDTO("ROLE_GUEST", "GUEST");
		mocMvc.perform(MockMvcRequestBuilders.post("/api.v1/authorities")
				.accept(MediaType.APPLICATION_JSON_UTF8)
				.content(TestUtil.writeAsString(payload))).andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isCreated());
	}

}
