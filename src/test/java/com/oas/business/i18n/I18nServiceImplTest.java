package com.oas.business.i18n;

import java.util.List;
import java.util.Locale;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.oas.business.admin.message.I18nRepository;
import com.oas.i18n.I18nService;
import com.oas.model.I18nMessage;

@RunWith(SpringRunner.class)
@SpringBootTest
public class I18nServiceImplTest {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private I18nService i18nService;
	
	@Autowired
	private I18nRepository i18nRepository;
	
	@Test
	public void testMultipleLanguage(){
		I18nMessage message = new I18nMessage();
		message.setCode("err.notfound");
		message.setLocale(Locale.ENGLISH.getLanguage());
		message.setMessage("Not Found");
		i18nRepository.saveAndFlush(message);
		
		I18nMessage messagevn = new I18nMessage();
		messagevn.setCode("err.notfound");
		messagevn.setLocale(Locale.JAPANESE.getLanguage());
		messagevn.setMessage("Khong tim Thay");
		i18nRepository.saveAndFlush(messagevn);
		List<I18nMessage> data = i18nRepository.findAll();
		log.info("\n>>>> {}", data);
		
		
		String msg = i18nService.getFromDb("err.notfound", Locale.ENGLISH); 
		Assertions.assertThat(msg).isEqualTo("Not Found");
		
	}
	
}
